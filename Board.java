import java.util.Random;
public class Board{
	//fields
	private Tile[][] grid; //toString will be a loop
	private final int boardSize;
	//constructor
	public Board(){
		Random rand = new Random();
		this.boardSize = 5;
		this.grid = new Tile[this.boardSize][this.boardSize];
		for(int i = 0; i < this.grid.length; i++){
			int randIndex = rand.nextInt(grid.length);
			for(int j = 0; j < this.boardSize; j++){
				if(j == randIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	//toString Override to print TicTakToe table
	public String toString(){
		String output = "";
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < this.boardSize; j++){
				output += grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	//Method that checks if token is allowed to be place & replaces blank if true
	public int placeToken(int row, int col){
		//Out of Bounds
		if((col >= this.boardSize || col < 0) || (row >= this.boardSize || row < 0)){
			return -2;
		}
		else if(grid[col][row] == Tile.CASTLE || grid[col][row] == Tile.WALL){
			return -1;
		}
		else if(grid[col][row] == Tile.HIDDEN_WALL){
			return 1;
		}
		else{
			grid[col][row] = Tile.CASTLE;
			return 0;
		}
	}
}