import java.util.Scanner;	
public class BoardGameApp{
	public static void main(String[] arg){
		Scanner reader = new Scanner(System.in);
		Board gameBoard = new Board();
		
		System.out.println("Welcome to Castles and Walls!");
		int numCastles = 7;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8){
			System.out.println(gameBoard);
			System.out.println("# of Castles: " + numCastles);
			System.out.println("# of Turns: " + turns);
			System.out.println("Enter the row and column you wish to place your castle");
			
			System.out.print("Row: "); 
			int choosenRow = Integer.parseInt(reader.nextLine()); 
			System.out.print("Column: ");
			int choosenColumn = Integer.parseInt(reader.nextLine());
			
			int placeToken = gameBoard.placeToken(choosenRow, choosenColumn);
			
			if(placeToken < 0){
				System.out.println("Invalid Inputs, Re enter column and row");
			}
			else if(placeToken == 1){
				System.out.println("There is a wall at that position");
			}
			else{
				System.out.println("Castle was placed successfully!");
				numCastles--;
				turns++;
			}
		}
		System.out.println(gameBoard);
		if(numCastles == 0){
			System.out.println("You Win!");
		}
		else{
			System.out.println("You Lose..");

		}
	}
}