public enum Tile{
	//Enum elements
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	//fields
	private String name;
	//Constructor
	private Tile(String name){
		this.name = name;
	}
	//Getters
	public String getName(){
		return this.name;
	}
	/*
		Personal Note: Why not use toString?
		Ans: Tedious to use when dealing w/ an unmanagable amount of constants
	*/
	
}